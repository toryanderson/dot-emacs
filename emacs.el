;;; .emacs.el  --- Emacs Init File -S*- lexical-binding: t -*-
;;; THIS FILE IS GENERATED

(setq vc-follow-symlinks t)

(setq user-emacs-directory "~/emacs/.emacs.d/")
(add-to-list 'load-path "~/emacs/.emacs.d/lisp")
(add-to-list 'load-path "~/emacs/.emacs.d/lisp/seq.el")
(setq custom-file "~/emacs/emacs-custom.el")

(require 'server)
(unless (server-running-p)
  (server-start))

(setq isearch-lax-whitespace nil)
(defalias 'yes-or-no-p 'y-or-n-p) ; stop asking "yes" http://www.emacswiki.org/emacs/YesOrNoP
(add-to-list 'auto-mode-alist '("\\.service\\'" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.path\\'" . conf-mode))

(setq straight-cache-autoloads t
      straight-check-for-modifications '(check-on-save find-when-checking)
      straight-repository-branch "develop"
      straight-use-package-by-default t
      use-package-always-ensure nil)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

;; https://github.com/raxod502/straight.el/issues/49#issuecomment-395979478
(defun straight-x-clean-unused-repos ()
  (interactive)
  (dolist (repo (straight--directory-files (straight--repos-dir)))
    (unless (or (straight--checkhash repo straight--repo-cache)
                (not (y-or-n-p (format "Delete repository %S?" repo))))
      (delete-directory (straight--repos-dir repo) 'recursive 'trash))))

(load-file "~/emacs/.emacs.d/lisp/tsa-misc.el")

(setq browse-pdf-generic-program (executable-find "emacsclient"))
(put 'scroll-left 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'downcase-region 'disabled nil)

;; my URL shortener, using my self-hosted yourls instant
(tsa/safe-load-file "~/emacs/.emacs.d/lisp/shorten.el")

(add-hook 'message-mode-hook 'footnote-mode) ; same footnote key sequence as orgmode
(add-hook 'footnote-mode-hook 
	  (lambda ()
	    (local-set-key (kbd "C-c C-x f") 'Footnote-add-footnote)))

(use-package anzu
  :delight
  :config (global-anzu-mode 1)
  (setq anzu-minimum-input-length 4))

(use-package apache-mode )

(use-package bbdb
   :demand t
  :config
  (setq bbdb-file "~/emacs/bbdb"))

(use-package bookmark+ ; (provide 'bookmark+-mac)
    :straight (bookmark+ :type git :host github :repo "emacsmirror/bookmark-plus")
    :demand t
    :bind (("C-x j j" . counsel-bookmark))
    :custom
    (bmkp-last-as-first-bookmark-file "/home/torysa/emacs/.emacs.d/bookmarks")
    (bmkp-a-mark '((t (:background "cyan" :foreground "black"))))

    :config
    (setq bmkp-default-handlers-for-file-types 
	  '(("\\.pdf$" . find-file)
	    ("\\.html$" . "firefox")))
    (defadvice bookmark-jump (after bookmark-jump activate)
      (let ((latest (bookmark-get-bookmark bookmark)))
	(setq bookmark-alist (delq latest bookmark-alist))
	(add-to-list 'bookmark-alist latest)))
    (setq bmkp-prompt-for-tags-flag nil
	  bookmark-version-control t))

(use-package bufler
  :delight '(:eval (if bufler-workspace-mode (concat "[buf:-" bufler-workspace-name "]") ""))
  :bind (("C-x C-b" . bufler)
	 :map bufler-list-mode-map
	 ("G" . tramp-cleanup-all-buffers))
  :config
  ;(bufler-mode t)
  (setf bufler-groups
	(bufler-defgroups
	  (group
	   ;; Subgroup collecting all named workspaces.
	   (auto-workspace))
	  ;; (group
	  ;;  (group-and "*Firefox*"
	  ;; 	      (mode-match "*EXWM*" (rx bos "EXWM"))
	  ;; 	      (name-match "*Firefox*" (rx bos "F :"))))
	  (group
	   (mode-match "*EXWM*" (rx bos "EXWM"))
	   (name-match "*Firefox*" (rx bos "F :")))
	  (group
	   ;; Subgroup collecting all `help-mode' and `info-mode' buffers.
	   (group-or "*Help/Info*"
		     (mode-match "*Help*" (rx bos "help-"))
		     (mode-match "*Info*" (rx bos "info-"))))
	  (group
	   ;; Subgroup collecting all special buffers (i.e. ones that are not
	   ;; file-backed), except `magit-status-mode' buffers (which are allowed to fall
	   ;; through to other groups, so they end up grouped with their project buffers).
	   (group-and "*Special*"
		      (lambda (buffer)
			(unless (or (funcall (mode-match "Magit" (rx bos "magit-status"))
					     buffer)
				    (funcall (mode-match "Dired" (rx bos "dired"))
					     buffer)
				    (funcall (auto-file) buffer))
			  "*Special*")))
	   (group
	    ;; Subgroup collecting these "special special" buffers
	    ;; separately for convenience.
	    (name-match "**Special**"
			(rx bos "*" (or "Messages" "Warnings" "scratch" "Backtrace") "*")))
	   (group
	    ;; Subgroup collecting all other Magit buffers, grouped by directory.
	    (mode-match "*Magit* (non-status)" (rx bos (or "magit" "forge") "-"))
	    (auto-directory))
	   ;; Subgroup for Helm buffers.
	   (mode-match "*Helm*" (rx bos "helm-"))
	   ;; Remaining special buffers are grouped automatically by mode.
	   (auto-mode))
	  ;; All buffers under "~/.emacs.d" (or wherever it is).
	  (dir user-emacs-directory)
	  (group
	   ;; Subgroup collecting buffers in `org-directory' (or "~/org" if
	   ;; `org-directory' is not yet defined).
	   (dir (if (bound-and-true-p org-directory)
		    org-directory
		  "~/org"))
	   (group
	    ;; Subgroup collecting indirect Org buffers, grouping them by file.
	    ;; This is very useful when used with `org-tree-to-indirect-buffer'.
	    (auto-indirect)
	    (auto-file))
	   ;; Group remaining buffers by whether they're file backed, then by mode.
	   (group-not "*special*" (auto-file))
	   (auto-mode))
	  (group
	   ;; Subgroup collecting buffers in a projectile project.
	   (auto-projectile))
	  (group
	   ;; Subgroup collecting buffers in a version-control project,
	   ;; grouping them by directory.
	   (auto-project))
	  ;; Group remaining buffers by directory, then major mode.
	  (auto-directory)
	  (auto-mode))))

(use-package iflipb
  :config
  (global-set-key [M-tab] 'iflipb-next-buffer)
  (global-set-key (kbd "M-S-TAB") 'iflipb-previous-buffer))

(use-package cider
  :bind (("C-c M-;" . cider-pprint-eval-last-sexp-to-comment))
  :config
  (setq cider-repl-use-clojure-font-lock t
	cider-font-lock-dynamically '(macro core function var)
	cider-default-cljs-repl 'figwheel
	cider-repl-display-help-banner nil
	cider-repl-use-pretty-printing t)
  (fset 'tsa/clojure-letvar-to-def
	(lambda (&optional arg)
	  "with cursor at a let-var, def it so you can proceed with repl debugging." 
	  (interactive "p") (kmacro-exec-ring-item (quote ([40 100 101 102 32 C-right C-right 134217734 134217734 134217734 24 5 67108911 67108911] 0 "%d")) arg)))
  (define-key clojure-mode-map (kbd "M-L") 'tsa/clojure-letvar-to-def))

(use-package clojure-mode
  :init
  (use-package flycheck-joker )

  :config
  (cider-auto-test-mode t)
  (defun my-clojure-mode-hook () 
    (highlight-phrase "TODO" 'clj-todo-face)
    (yas-minor-mode 1) 
    (cljr-add-keybindings-with-prefix "C-c C-m"))
  (add-hook 'clojure-mode-hook #'my-clojure-mode-hook)
  (add-hook 'clojure-mode-hook 'flycheck-mode)
  (use-package flycheck-clj-kondo 
    :config
    (dolist (checkers '((clj-kondo-clj . clojure-joker)
			(clj-kondo-cljs . clojurescript-joker)
			(clj-kondo-cljc . clojure-joker)))
      (flycheck-add-next-checker (car checkers) (cons 'error (cdr checkers))))))

(use-package clojure-mode-extra-font-locking
  :requires clojure-mode)

(use-package col-highlight)

(use-package clj-refactor)

(use-package command-log-mode
  :config
  (add-hook 'clojure-mode-hook 'command-log-mode))

(use-package company
     :delight company-mode
;     :bind (("TAB" . company-indent-or-complete-common))
     :defer t
     :config
     (global-company-mode)
     ;; (define-key company-mode-map (kbd "C-:") 'helm-company)
     ;; (define-key company-active-map (kbd "C-:") 'helm-company)
     (setq company-idle-delay 0.3))

   (use-package company-quickhelp
;     :ensure pos-tip
     :demand t
     :config
     (company-quickhelp-mode 1)
     (setq company-quickhelp-delay 0.5)
     (add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode)))

(use-package crontab-mode)

(use-package csv-mode)

(use-package dired+     
  :custom (dired-listing-switches "-alh")
  :bind (:map dired-mode-map 
	      ("C-c C-r" . dired-toggle-read-only))
  :config
  (add-hook 'dired-mode-hook
	    (lambda ()
	      (define-key dired-mode-map (kbd "<return>")
		'dired-find-alternate-file) ; was dired-advertised-find-file
	      (define-key dired-mode-map (kbd "^")
		(lambda () (interactive) (find-alternate-file "..")))
					; was dired-up-directory
	      ))
  (setq dired-guess-shell-alist-user
	(list (list "\\.*$" "xdg-open");; fixed rule
       ;; possibly more rules...
	      ))
  (put 'dired-find-alternate-file 'disabled nil))

(defun dired-sort-toggle ()
  "This is a redefinition of the fn from dired.el. Normally,
dired sorts on either name or time, and you can swap between them
with the s key.  This function one sets sorting on name, size,
time, and extension. Cycling works the same.
"
  (setq dired-actual-switches
        (let (case-fold-search)
          (cond
           ((string-match " " dired-actual-switches) ;; contains a space
            ;; New toggle scheme: add/remove a trailing " -t" " -S",
            ;; or " -U"
            ;; -t = sort by time (date)
            ;; -S = sort by size
            ;; -X = sort by extension

            (cond

             ((string-match " -t\\'" dired-actual-switches)
              (concat
               (substring dired-actual-switches 0 (match-beginning 0))
               " -X"))

             ((string-match " -X\\'" dired-actual-switches)
              (concat
               (substring dired-actual-switches 0 (match-beginning 0))
               " -S"))

             ((string-match " -S\\'" dired-actual-switches)
              (substring dired-actual-switches 0 (match-beginning 0)))

             (t
              (concat dired-actual-switches " -t"))))

           (t
            ;; old toggle scheme: look for a sorting switch, one of [tUXS]
            ;; and switch between them. Assume there is only ONE present.
            (let* ((old-sorting-switch
                    (if (string-match (concat "[t" dired-ls-sorting-switches "]")
                                      dired-actual-switches)
                        (substring dired-actual-switches (match-beginning 0)
                                   (match-end 0))
                      ""))

                   (new-sorting-switch
                    (cond
                     ((string= old-sorting-switch "t") "X")
                     ((string= old-sorting-switch "X") "S")
                     ((string= old-sorting-switch "S") "")
                     (t "t"))))
              (concat
               "-l"
               ;; strip -l and any sorting switches
               (dired-replace-in-string (concat "[-lt"
                                                dired-ls-sorting-switches "]")
                                        ""
                                        dired-actual-switches)
               new-sorting-switch))))))

  (dired-sort-set-modeline)
  (revert-buffer))

(defun dired-sort-set-modeline ()
 "This is a redefinition of the fn from `dired.el'. This one
properly provides the modeline in dired mode, supporting the new
search modes defined in the new `dired-sort-toggle'.
"
  ;; Set modeline display according to dired-actual-switches.
  ;; Modeline display of "by name" or "by date" guarantees the user a
  ;; match with the corresponding regexps.  Non-matching switches are
  ;; shown literally.
  (when (eq major-mode 'dired-mode)
    (setq mode-name
          (let (case-fold-search)
            (cond ((string-match "^-[^t]*t[^t]*$" dired-actual-switches)
                   "Dired by time")
                  ((string-match "^-[^X]*X[^X]*$" dired-actual-switches)
                   "Dired by ext")
                  ((string-match "^-[^S]*S[^S]*$" dired-actual-switches)
                   "Dired by sz")
                  ((string-match "^-[^SXUt]*$" dired-actual-switches)
                   "Dired by name")
                  (t
                   (concat "Dired " dired-actual-switches)))))
    (force-mode-line-update)))

(use-package diredfl 
  :config
  (add-hook 'dired-mode-hook 'diredfl-mode)
  :custom
  (diredfl-dir-name '((t (:foreground "#3679D8" :box (:line-width 2 :color "grey75" :style released-button)))))
  (diredfl-dir-priv '((t (:foreground "#3679D8" :underline t))))
  (diredfl-exec-priv '((t (:background "#79D836" :foreground "black"))))
  (diredfl-read-priv '((t (:background "#D8B941" :foreground "black"))))
  (diredfl-write-priv '((t (:background "#D83441" :foreground "black")))))

(use-package dired-rainbow 
:config
  (progn
    (dired-rainbow-define-chmod directory "#6cb2eb" "d.*")
    (dired-rainbow-define html "#eb5286" ("css" "less" "sass" "scss" "htm" "html" "jhtm" "mht" "eml" "mustache" "xhtml"))
    (dired-rainbow-define xml "#f2d024" ("xml" "xsd" "xsl" "xslt" "wsdl" "bib" "json" "msg" "pgn" "rss" "yaml" "yml" "rdata"))
    (dired-rainbow-define document "#9561e2" ("docm" "doc" "docx" "odb" "odt" "pdb" "pdf" "ps" "rtf" "djvu" "epub" "odp" "ppt" "pptx"))
    (dired-rainbow-define markdown "#ffed4a" ("org" "etx" "info" "markdown" "md" "mkd" "nfo" "pod" "rst" "tex" "textfile" "txt"))
    (dired-rainbow-define database "#6574cd" ("xlsx" "xls" "csv" "accdb" "db" "mdb" "sqlite" "nc"))
    (dired-rainbow-define media "#de751f" ("mp3" "mp4" "MP3" "MP4" "avi" "mpeg" "mpg" "flv" "ogg" "mov" "mid" "midi" "wav" "aiff" "flac"))
    (dired-rainbow-define image "#f66d9b" ("tiff" "tif" "cdr" "gif" "ico" "jpeg" "jpg" "png" "psd" "eps" "svg"))
    (dired-rainbow-define log "#c17d11" ("log"))
    (dired-rainbow-define shell "#f6993f" ("awk" "bash" "bat" "sed" "sh" "zsh" "vim"))
    (dired-rainbow-define interpreted "#38c172" ("py" "ipynb" "rb" "pl" "t" "msql" "mysql" "pgsql" "sql" "r" "clj" "cljs" "scala" "js"))
    (dired-rainbow-define compiled "#4dc0b5" ("asm" "cl" "lisp" "el" "c" "h" "c++" "h++" "hpp" "hxx" "m" "cc" "cs" "cp" "cpp" "go" "f" "for" "ftn" "f90" "f95" "f03" "f08" "s" "rs" "hi" "hs" "pyc" ".java"))
    (dired-rainbow-define executable "#8cc4ff" ("exe" "msi"))
    (dired-rainbow-define compressed "#51d88a" ("7z" "zip" "bz2" "tgz" "txz" "gz" "xz" "z" "Z" "jar" "war" "ear" "rar" "sar" "xpi" "apk" "xz" "tar"))
    (dired-rainbow-define packaged "#faad63" ("deb" "rpm" "apk" "jad" "jar" "cab" "pak" "pk3" "vdf" "vpk" "bsp"))
    (dired-rainbow-define encrypted "#ffed4a" ("gpg" "pgp" "asc" "bfe" "enc" "signature" "sig" "p12" "pem"))
    (dired-rainbow-define fonts "#6cb2eb" ("afm" "fon" "fnt" "pfb" "pfm" "ttf" "otf"))
    (dired-rainbow-define partition "#e3342f" ("dmg" "iso" "bin" "nrg" "qcow" "toast" "vcd" "vmdk" "bak"))
    (dired-rainbow-define vc "#0074d9" ("git" "gitignore" "gitattributes" "gitmodules"))
    (dired-rainbow-define-chmod executable-unix "#38c172" "-.*x.*")
    ))

;; Ibuffer-style filtering and saved filter groups (persistent, unlike narrow)
(use-package dired-filter)

;; Smarter dealing with subdirectories
(use-package dired-subtree)

;; Live filtering of dired
(use-package dired-narrow
  :config 
  (bind-keys
   :map dired-mode-map
   ("C-c n" . dired-narrow)))

(use-package easy-kill
  :config
  (global-set-key [remap kill-ring-save] 'easy-kill)
  (global-set-key [remap mark-sexp] 'easy-mark))

;; GOLDEN PACKAGE for scholarship
(use-package ebib
  :bind (("C-c e" . ebib))
  :config
  (setq
   ebib-bib-search-dirs (quote ("~/Documents/School/Story_Theory/Bib"))
   ebib-file-associations (quote (("pdf" . "/usr/bin/xdg-open") ("ps" . "gv")))
   ebib-file-search-dirs (quote ("~/Documents/School/Story_Theory/Readings"))
   ebib-index-display-fields (quote ("title"))
   ebib-preload-bib-files (quote ("story.bib"))
   ebib-bib-search-dirs (quote ("~/Documents/School/Story_Theory/Bib"))
   ebib-file-associations (quote (("pdf" . "/usr/bin/xdg-open") ("ps" . "gv")))
   ebib-file-search-dirs (quote ("~/Documents/School/Story_Theory/Readings"))
   ebib-index-display-fields (quote ("title"))
   ebib-preload-bib-files (quote ("story.bib"))))

(use-package ediff)

(use-package elfeed
  :bind (:map elfeed-search-mode-map
	      ("B" . tsa/elfeed-search-browse-background-url)
	      :map elfeed-show-mode-map
	      ("B" . tsa/elfeed-search-browse-background-url))
  ;; :bind (:map elfeed-search-mode-map
  ;; 	      ("A" . bjm/elfeed-show-all)
  ;; 	      ("E" . bjm/elfeed-show-emacs)
  ;; 	      ("D" . bjm/elfeed-show-daily)
  ;; 	      ("q" . bjm/elfeed-save-db-and-bury))
  :config
  (defun tsa/elfeed-search-browse-background-url ()
    "Open current `elfeed' entry (or region entries) in browser without losing focus."
    (interactive)
    (let ((entries (elfeed-search-selected)))
      (mapc (lambda (entry)
	      (start-process (concat "xdg-open " (elfeed-entry-link entry))
			     nil "firefox" "-new-tab" (elfeed-entry-link entry))
	      (elfeed-untag entry 'unread)
	      (elfeed-search-update-entry entry))
	    entries)
      (unless (or elfeed-search-remain-on-entry (use-region-p))
	(forward-line)))))

(use-package elfeed-goodies
  :config
  (elfeed-goodies/setup))

(use-package elfeed-org
  :config
  (elfeed-org)
  (setq rmh-elfeed-org-files (list "~/emacs/org/elfeed.org")))

(use-package erc
  :config
  (use-package erc-hl-nicks ) ;; highlight names in erc
  (setq erc-server "irc.freenode.net"
	erc-port 6667
	erc-nick "WorldsEndless"
	erc-user-full-name "Tory S. A." ; do I really want this here? 
	erc-prompt-for-password ()
	erc-hide-list '("JOIN" "PART" "QUIT")
	erc-autojoin-channels-alist '(("freenode.net" "#emacs" "#erc" "#gnus" "#clojure" "#clojurescript"))
	erc-fill-column 72
	erc-fill-mode nil))

(use-package esup
  :commands esup)

;; good package to make a file reading mode much nicer
(use-package god-mode
  :config
  (global-set-key (kbd "<escape>") 'god-mode-all)
  (setq god-exempt-major-modes nil
	god-exempt-predicates nil)
  (define-key god-local-mode-map (kbd ".") 'repeat)
  (require 'god-mode-isearch)
  (define-key isearch-mode-map (kbd "<escape>") 'god-mode-isearch-activate)
  (define-key god-mode-isearch-map (kbd "<escape>") 'god-mode-isearch-disable)
  (define-key god-local-mode-map (kbd "<backspace>") 'scroll-down-command)
  (defun my-update-cursor ()
    (setq cursor-type (if (or god-local-mode buffer-read-only)
			  'hbar
			'box)))
  (add-hook 'god-mode-enabled-hook 'my-update-cursor)
  (add-hook 'god-mode-disabled-hook 'my-update-cursor))

(use-package hi-lock
  :config
  (global-hi-lock-mode 1))

;; GOLDEN PACKAGE
(use-package hydra
  :config (tsa/safe-load-file "~/emacs/.emacs.d/lisp/tsa-hydra.el"))

(use-package ivy
;	  :after helm
	  :delight
	  :custom
	  (ivy-current-match '((t (:extend t :background "MediumBlue" :weight bold))))
	  (counsel-find-file-at-point t)
	  (max-mini-window-height 1.0)
	  (resize-mini-windows t)
	  (ivy-fixed-height-minibuffer nil)
	  (ivy-read-action-function 'ivy-read-action-ivy)
	  :bind (("C-h z" . ivy-resume))
	  :config
	  (ivy-mode 1)
	  (setq ivy-use-virtual-buffers t)
	  (setq enable-recursive-minibuffers t)
	  ;; enable this if you want `swiper' to use it
	  ;; (setq search-default-mode #'char-fold-to-regexp)
	  ;; (global-set-key (kbd "C-s") 'swiper-isearch)
	  ;; (global-set-key (kbd "C-r") 'swiper-isearch)
	  (global-set-key (kbd "C-c C-r") 'ivy-resume)
	  (global-set-key (kbd "<f6>") 'ivy-resume)
	  (global-set-key (kbd "M-x") 'counsel-M-x)
	  (global-set-key (kbd "C-x C-f") 'counsel-find-file)
	  ;; (global-set-key (kbd "<f1> f") 'counsel-describe-function)
	  ;; (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
	  ;; (global-set-key (kbd "<f1> o") 'counsel-describe-symbol)
	  ;; (global-set-key (kbd "<f1> l") 'counsel-find-library)
	  (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
	  (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
	  (global-set-key (kbd "C-x b") 'bufler-switch-buffer)
	  (global-set-key (kbd "C-c g") 'counsel-git)
	  (global-set-key (kbd "C-c j") 'counsel-git-grep)
	  (global-set-key (kbd "C-x l") 'counsel-locate)
	  (global-set-key (kbd "C-h k") 'counsel-descbinds)
	  (global-set-key (kbd "M-y") 'counsel-yank-pop)
	  (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
	  (define-key org-mode-map (kbd "C-c s") 'counsel-outline)
	  (setq projectile-completion-system 'ivy)
    )

(use-package ivy-hydra)

(use-package swiper)

(use-package counsel
  :custom (ivy-count-format "(%d/%d) "))

(use-package ivy-rich
  :config
  (ivy-rich-mode 1)
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line))

(use-package counsel-projectile
  :bind (:map projectile-mode-map
	      ("C-c p" . 'projectile-command-map)))

; (all-the-icons-install-fonts)
  (use-package all-the-icons-ivy-rich
    :custom (all-the-icons-ivy-rich-mode t)
    :config (setq inhibit-compacting-font-caches t))

(use-package counsel-notmuch)

(use-package ivy-bibtex
  :bind (("C-c r" . ivy-bibtex))
  :custom
  (bibtex-completion-bibliography '("~/Story/Bib/story.bib"))
  (bibtex-completion-library-path '("~/Story/Readings"))
  :config
  (add-to-list 'ivy-re-builders-alist '(ivy-bibtex . ivy--regex-ignore-order)))

(use-package amx
  :custom (amx-mode t))

(defun tsa/format-org-template (template)
  "Format the entries of one of `org-capture-templates` for Ivy selection"
  (cons (nth 1 template) (nth 0 template)))

(defun tsa/ivy-select-org-capture-template ()
  "Select an org-capture-template with ivy"
  (interactive)  
  (ivy-read "Capture template: "
	    (mapcar 'tsa/format-org-template org-capture-templates)
	    :history 'counsel-org-capture-templates-history
	    :require-match t
	    :caller 'tsa/ivy-select-org-capture-template
	    :action (lambda (template)
		      (let ((target (cdr template)))
			(org-capture nil target)))))

(use-package free-keys)

(use-package which-key
  :delight
  :config
  (which-key-mode))

;; Famous for emacs Latex
(use-package tex
  :straight auctex
  :config
  (setq TeX-PDF-mode t
	TeX-global-PDF-mode t
	TeX-auto-save t
	TeX-parse-self t
	TeX-view-program-list '(("Okular" "okular %o")
				("Emacs" "emacsclient %o"))
	TeX-view-program-selection '((output-pdf "Emacs"))
	LaTeX-command-style '(("" "%(PDF)%(latex) -file-line-error %S%(PDFout)")))
  (setq-default TeX-master t) ; All master files called "master".
					;(setq TeX-master 'shared) ; shared LaTeX: ask for master file
  (defadvice ispell-send-string (before kill-quotes activate)
    (setq string (replace-regexp-in-string "''" "  " string))))

;; GOLDEN PAKAGE
(use-package magit
   :demand
  :bind (:map magit-section-mode-map
	      ([M-tab] . iflipb-next-buffer)
	      ("M-TAB" . iflipb-next-buffer)
	      ("M-S-TAB" . iflipb-previous-buffer)
	      :map magit-mode-map
	      ([M-tab] . iflipb-next-buffer)
	      ("M-TAB" . iflipb-next-buffer)
	      ("M-S-TAB" . iflipb-previous-buffer))
  :config
  (add-hook 'ediff-prepare-buffer-hook #'show-all) ;; Expand orgmode files before ediffing them
  (global-magit-file-mode)
  (global-set-key (kbd "C-x g") 'magit-status)
  (setq magit-diff-use-overlays nil))

(use-package markdown-mode
  :config
  ;;(use-package "ac-html-csswatcher")
  ;; (use-package "company-web")
  ;; (use-package "helm-company")
  ;; (define-key company-mode-map (kbd "C-:") 'helm-company)
  ;; (define-key company-active-map (kbd "C-:") 'helm-company)
  (setq company-idle-delay 0.3))

(use-package hide-mode-line
  :bind (("C-`" . hide-mode-line-mode)
	 ("C-~" . global-hide-mode-line-mode))
  :config
  (add-hook 'calculator-mode-hook (lambda () (hide-mode-line-mode t))))

(use-package multiple-cursors)

(use-package org
  :demand t
  :delight
  :bind (:map org-mode-map
	      ("C-c ." . org-time-stamp)
	      ("M-[" . org-metaleft)
	      ("M-]" . org-metaright)
	      ("M-{" . org-shiftmetaleft)
	      ("M-}" . org-shiftmetaright))

  :custom  
  (org-src-fontify-natively t)
  (org-latex-pdf-process
   '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
     "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
     "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

  :config
  ;(use-package org-capture)
  (tsa/safe-load-file "~/emacs/.emacs.d/lisp/tsa-org.el") ;; TODO move to its own use-package
  ;; (define-key org-capture-mode-map (kbd "C-c TAB") 'org-set-tags)
  (setq org-drawers (quote ("PROPERTIES" "HIDDEN" "NOTE" "COMMENT" "CODE" "CLOCK" "LOGBOOK"))
	CLOCK_INTO_DRAWER t
	org-directory "~/emacs/org/"
	org-enforce-todo-dependencies t
	org-enforce-todo-checkbox-dependencies t)
  (defun tsa/org-auto-check ()
    "Update all the status on an org tree so [0/3] is updated to [0/4]"
    (org-update-statistics-cookies "ALL"))

  (add-hook 'org-mode-hook (lambda () (setq-local next-error-function nil)))
  (add-hook 'org-capture-before-finalize-hook 'tsa/org-auto-check))

(defun std::org-edit-src-exit ()
  (interactive)
  (-let [w (selected-window)]
    (org-edit-src-exit)
    (delete-window w)))

(defun tsa/org-clear-appts ()
  (interactive)
  (let ((appt-count (if (boundp 'appt-time-msg-list)
			(length appt-time-msg-list)
		      0)))
    (setq appt-time-msg-list nil)
    (message (format "Cleared %d events" appt-count))))

(defun tsa/org-appts (arg)
  (interactive "P")
  (cond ((equal arg '(4))
	 (progn
	   (tsa/org-clear-appts)
	   (message "Appointments cleared")))
	(t (progn
	   (tsa/org-clear-appts)
	   (org-agenda-to-appt)))))

  (use-package org-agenda
    :straight (:type built-in)
    :custom
    (org-agenda-clocking '((t (:background "gold" :foreground "black"))))
    (org-agenda-dimmed-todo-face '((t (:foreground "dimgray"))))

    :bind (:map org-agenda-mode-map
		("P" . tsa/org-appts)))

(use-package org-ref 
  :custom
  (org-ref-insert-cite-key "C-c )")
  (org-ref-bibtex-completion-actions nil) ;; don't change default
  (org-ref-bibliography-notes "~/Story/Bib/Annotations/notes.org")
  (org-ref-default-bibliography '("~/Story/Bib/story.bib"))
  (org-ref-pdf-directory "~/Story/Readings/"))

(use-package org-alert

  :custom
  (alert-default-style 'libnotify)
  (org-alert-interval 500)
  :config
  (setq org-alert-enable t))

(use-package org-super-agenda
  :delight
  :custom
  (org-super-agenda-mode t)
  :config
  (setq org-super-agenda-groups
	'((:name "Agenda"
		 :time-grid t)
	  (:name "Work"		 
		 :tag "ODH")
	  (:name "School"
		 :tag "CLASS")
	  (:name "PhD"
		 :tag "PhD")
	  (:name "Priesthood"
		 :tag "PRIESTHOOD")
	  (:name "Non-Work"
		 :not (:tag "ODH"))
	  (:name "Backburner"
		 :priority<= "B"
		 :order 1))))

(use-package org-tempo
  :straight (:type built-in)
  :custom
  (org-structure-template-alist
   '(("s" . "src")
     ("e" . "example")
     ("q" . "quote")
     ("v" . "verse")
     ("V" . "verbatim")
     ("c" . "center")
     ("C" . "comment")
     ("l" . "latex")
     ("h" . "html"))))

(use-package ox-latex
  :straight (:type built-in)
  :custom
  (org-latex-listings 'minted)
  :config
  (add-to-list 'org-latex-packages-alist '("" "minted")))

(use-package ox-gfm)

(use-package ox-md
  :straight (:type built-in)
  :config
  ;; http://www.holgerschurig.de/en/emacs-blog-from-org-to-hugo/
  (defvar hugo-default-banner "https://images.toryanderson.com/default_images/tsa_0.png"
    "Path to my default banner for my blogs")
  (defvar-local hugo-content-dir "~/Site/Sites/Blogs/"
    "Path to Hugo's content directory")
  (defvar-local hugo-publish-script nil
    "Path to the given blog's publish script, which runs hugo and uploads to the server")

(defun hugo-ensure-property (property)
    "Make sure that a property exists. If not, it will be created.
Returns the property name if the property has been created,
otherwise nil."
    (if (org-entry-get nil property)
	nil
      (progn (org-entry-put nil property "")
	     property)))

(defun hugo-ensure-properties ()
  "This ensures that several properties exists. If not, these
properties will be created in an empty form. In this case, the
drawer will also be opened and the cursor will be positioned
at the first element that needs to be filled.

Returns list of properties that still must be filled in"
  (require 'dash)
  (let* ((current-time (format-time-string (org-time-stamp-format t t) (org-current-time)))
	 (org-title (nth 4 (org-heading-components)))
	 (title-to-slug (downcase
			 (replace-regexp-in-string "[?:,.'#;=+&\"/\]" ""
						   (replace-regexp-in-string " +" "-" org-title))))
	 (default-file-name (concat "posts/"
				    title-to-slug
				    ".md"))
	 (tag-str (string-join (org-get-local-tags) ", "))
	 first)
    (save-excursion
      (unless (org-entry-get nil "TITLE")
	(org-entry-put nil "TITLE" org-title))
      (unless (org-entry-get nil "HUGO_TAGS")
	(org-entry-put nil "HUGO_TAGS" tag-str))
      (unless (org-entry-get nil "HUGO_TOPICS")
	(org-entry-put nil "HUGO_TOPICS" tag-str))
      (unless (org-entry-get nil "HUGO_FILE")
	(org-entry-put nil "HUGO_FILE" default-file-name))
      (unless (org-entry-get nil "HUGO_DATE")
	(org-entry-put nil "HUGO_DATE" current-time))
      (unless (org-entry-get nil "HUGO_BANNER")
	(org-entry-put nil "HUGO_BANNER" hugo-default-banner)))
    (when first
      (goto-char (org-entry-beginning-position))
      ;; The following opens the drawer
      (forward-line 1)
      (beginning-of-line 1)
      (when (looking-at org-drawer-regexp)
	(org-flag-drawer nil))
      ;; And now move to the drawer property
      (search-forward (concat ":" first ":"))
      (end-of-line))
    first))

(defun hugo ()
  (interactive)
  (unless (hugo-ensure-properties)
    (let* ((banner-loc (org-entry-get nil "HUGO_BANNER"))
	   (title    (concat "title = \"" (org-entry-get nil "TITLE") "\"\n"))
	   (date     (concat "date = \"" (format-time-string "%Y-%m-%d" (apply 'encode-time (org-parse-time-string (org-entry-get nil "HUGO_DATE"))) t) "\"\n"))
	   (topics   (concat "topics = [ \"" (mapconcat 'identity (split-string (org-entry-get nil "HUGO_TOPICS") "\\( *, *\\)" t) "\", \"") "\" ]\n"))
	   (tags     (concat "tags = [ \"" (mapconcat 'identity (split-string (org-entry-get nil "HUGO_TAGS") "\\( *, *\\)" t) "\", \"") "\" ]\n"))
	   (banner (concat "banner = \"" banner-loc "\"\n"))
	   (images (concat "images = [ \"" (mapconcat 'identity (split-string (org-entry-get nil "HUGO_BANNER") "\\( *, *\\)" t) "\", \"") "\" ]\n"))
	   (twitter (s-join "\n" (list 
				  "[twitter]"
				  "card = \"summary\""
				  "site = \"@twittername\""
				  "title = \"Twitter card title\""
				  "description = \"Twitter Description\""
				  (concat "image = \"" banner-loc "\"" ))))

	   (fm (concat "+++\n"
		       title
		       date
		       tags
		       topics
		       banner
		       images
		       twitter
		       "\n+++\n\n"))
	   (file     (org-entry-get nil "HUGO_FILE"))
	   (coding-system-for-write buffer-file-coding-system)
	   (backend  'md)
	   (blog))
      (if (require 'ox-gfm nil t)
	  (setq backend 'gfm)
	(require 'ox-md))
      (setq blog (org-export-as backend t))
      ;; Normalize save file path
      (unless (string-match "^[/~]" file)
	(setq file (concat hugo-content-dir file))
	(unless (string-match "\\.md$" file)
	  (setq file (concat file ".md")))
	;; save markdown
	(with-temp-buffer
	  (insert fm)
	  (insert blog)
	  (untabify (point-min) (point-max))
	  (write-file file)
	  (message "Exported to %s" file))))))

(defun hugo-publish-up ()
  (interactive)
  (if (boundp 'hugo-publish-script)
      (progn
	(shell-command hugo-publish-script nil)
	(bury-buffer "*Shell Command Output*"))
    (message "hugo-publish-script not defined for this buffer")))

(defun hugo-total ()
  (interactive)
  (hugo)
  (hugo-publish-up)
  (org-todo 'done))
  ) ;; end ox-md

(use-package smartparens
    :demand t
    :bind (("C-<f5>" . smartparens-mode))
    :custom
    (sp-show-pair-enclosing '((t (:inherit highlight :background "orange red"))))
    (sp-show-pair-match-face '((t (:background "#Ff4500" :foreground "black" :weight ultra-bold))))

    :config
    (show-smartparens-global-mode)
    (sp-use-paredit-bindings)
    (add-hook 'emacs-lisp-mode-hook 'turn-on-smartparens-strict-mode)
    (add-hook 'clojure-mode-hook 'turn-on-smartparens-strict-mode)
    (add-hook 'cider-repl-mode-hook #'turn-on-smartparens-strict-mode)
    (add-hook 'message-mode-hook 'turn-off-smartparens-mode)
    (add-hook 'org-mode-hook 'turn-off-smartparens-mode)
    (bind-keys
     :map smartparens-strict-mode-map
     (";" . sp-comment)
     ("M-f" . sp-forward-symbol)
     ("M-b" . sp-backward-symbol)
     ("M-a" . sp-beginning-of-sexp)
     ("M-e" . sp-end-of-sexp)))

(use-package smartparens-config
       :straight (:type built-in))

(use-package paren
  :config
  (show-paren-mode 1))

(use-package pdf-tools

  :demand t
  :bind (:map pdf-view-mode-map
	      ("G" . pdf-view-goto-page))
  :config
  (setq pdf-info-epdfinfo-program "~/.epdfinfo")
  (pdf-loader-install 'no-query)
  ;(pdf-tools-install 'no-query)
  (setq pdf-view-continuous t ; don't automatically jump to next page
	pdf-misc-print-program "/usr/bin/lp"
	pdf-misc-print-program-args '("-o fit-to-page"
				       "-o media=letter"
				       "-o sides=two-sided-long-edge"))

  (add-hook 'pdf-view-mode-hook 'auto-revert-mode)
  (define-pdf-cache-function pagelabels)
  (defun pdf-view-page-number ()
    (interactive)
    (if (called-interactively-p)
	(message "[pg %s/%s]"
		 (number-to-string (pdf-view-current-page))
		 (number-to-string (pdf-cache-number-of-pages)))
      (format  "[pg %s/%s]"
	       (number-to-string (pdf-view-current-page))
	       (number-to-string (pdf-cache-number-of-pages))))))

(use-package prism

  :bind (("C-c p" . prism-mode))
  :config
  (prism-set-colors :num 16
		    :desaturations (cl-loop for i from 0 below 16
					    collect (* i 2.5))
		    :lightens (cl-loop for i from 0 below 16
				       collect (* i 2.5))
		    :colors (list "dodgerblue" "medium sea green" "sandy brown")

		    :comments-fn
		    (lambda (color)
		      (prism-blend color
				   (face-attribute 'font-lock-comment-face :foreground) 0.25))

		    :strings-fn
		    (lambda (color)
		      (prism-blend color "white" 0.5))))

(use-package projectile
  :delight '(:eval (concat " [P: " (projectile-project-name) "]"))
  :custom
  (projectile-completion-system 'ivy)
  (projectile-switch-project-action 'counsel-projectile)
  :config
  (projectile-global-mode)
  (define-key projectile-command-map (kbd "s g") 'counsel-projectile-grep))

(add-hook 'python-mode-hook (lambda ()
			     (setq-local fill-column 79)
			     (auto-fill-mode)))

(use-package lsp-mode

  :hook (python-mode . lsp)

  :custom
  (help-at-pt-timer-delay 1)
  (help-at-pt-display-when-idle '(flymake-diagnostic))

  :commands lsp
  :config   
  (use-package lsp-ui  :commands lsp-ui-mode)
  (use-package company-lsp  :commands company-lsp)
  ;(use-package helm-lsp  :commands helm-lsp-workspace-symbol)
  (use-package dap-mode )
  (use-package dap-python))

(use-package rainbow-delimiters

  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package rainbow-identifiers

  :config
  (add-hook 'prog-mode-hook 'rainbow-identifiers-mode))

;; handy for CSS
(use-package rainbow-mode
  )

(use-package recentf
  :bind (("C-x C-r" . counsel-recentf))
  :config
  (setq recentf-max-menu-items 100)
  (recentf-mode 1))

(use-package epa-file
  :straight (:type built-in)
  :config
  (setq epa-pinentry-mode 'loopback))

(use-package shell
  :custom
  (comint-completion-addsuffix nil)
  :config ;http://stackoverflow.com/questions/704616/something-wrong-with-emacs-shell
  (autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)

  (add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
  (add-to-list 'display-buffer-alist
	       '("^\\*shell\\*$" . (display-buffer-same-window)))) ;; don't open shell in a new window

;; (use-package bash-completion
;;   :config
;;   (bash-completion-setup))

(use-package ispell
  :config
  (setq ispell-program-name "hunspell"
	ispell-dictionary "en_US"
	ispell-alternate-dictionary "en_US"
	ispell-complete-word-dict "~/.hunspell_en_US"))

(use-package flyspell-correct
  
    ;; :config
    ;; (global-set-key (kbd "<f12>") #'flyspell-correct-wrapper)
    ;; (global-set-key (kbd "S-<f12>") 'flyspell-buffer)
    )

  ;; (defun flyspell-correct-helm-stop ()
  ;;   (interactive)
  ;;   (helm-exit-with-action
  ;;    (lambda (_) (setq flyspell-correct-helm--result (cons 'stop "")))))

  ;; (use-package flyspell-correct-helm
  ;;   :init
  ;;   (setq flyspell-correct-interface #'flyspell-correct-helm)
  ;; 					;(define-key flyspell-correct-helm-map (kbd "C-g") #'flyspell-correct-helm-stop)
  ;;   ;; flyspell-correct-helm-map doesn't exist
  ;;   )

  (defun flyspell-correct-ivy-stop ()
    (interactive)
    (ivy-exit-with-action
     (lambda (_) (setq flyspell-correct-ivy--result (cons 'stop "")))))

  (use-package flyspell-correct-ivy
    :init
    (setq flyspell-correct-interface #'flyspell-correct-ivy)
    ;(define-key flyspell-correct-ivy-map (kbd "C-g") #'flyspell-correct-ivy-stop)
)

(use-package sql
  :config
  (setq sql-mysql-login-params (append sql-mysql-login-params '(port)))
  (setq sql-port 3306) ;; default MySQL port
  (setq sql-postgres-login-params
	'((user :default "torysa")
	  (database :default "torysa")
	  (server :default "localhost")
	  (port :default 5432))))

(use-package string-inflection

  :bind (("C-c C-u" . string-inflection-all-cycle)))

(use-package ace-window

  ;; :bind (("s-<tab>" . ace-window))
  )

(use-package ace-popup-menu

  :config
  (ace-popup-menu-mode 1))

(use-package ace-jump-mode
  :bind (("C-c SPC" . ace-jump-mode))
  :config
  (setq ace-jump-mode-case-fold nil  ;; case sensitive
	ace-isearch-use-function-from-isearch nil)
  (setq ace-jump-mode-submode-list '(ace-jump-line-mode ace-jump-char-mode ace-jump-word-mode) ;; complementary to ace-isearch
	ace-jump-mode-scope 'frame))

(use-package ace-isearch
  :demand t
  :delight
  :bind (:map isearch-mode-map 
	      ("M-i" . ace-isearch-swiper-from-isearch))
  :custom
  (ace-isearch-function 'avy-goto-char)
  (ace-isearch-function-from-isearch 'ace-isearch-swiper-from-isearch)
  (ace-isearch-input-idle-delay 0.2)
  (ace-isearch-input-length 9)
  (ace-isearch-use-ace-jump (quote printing-char))
  (ace-isearch-use-function-from-isearch t)
  (ace-isearch-use-jump (quote printing-char))
  (global-ace-isearch-mode +1))

(use-package ace-jump-zap

  :bind (("M-z" . ace-jump-zap-to-char))
  :config
  (setq ajz/zap-function 'kill-region))

(use-package windmove

  :config
  (setq windmove-default-keybindings t)
  (setq max-specpdl-size 10000))

(use-package winner
  :config
  (winner-mode 1))

(use-package delight)

(use-package telephone-line
  :after winum
  :custom 
  (telephone-line-primary-left-separator 'telephone-line-cubed-left)
  (telephone-line-secondary-left-separator 'telephone-line-cubed-hollow-left)
  (telephone-line-primary-right-separator 'telephone-line-cubed-right)
  (telephone-line-secondary-right-separator 'telephone-line-cubed-hollow-right)
  (telephone-line-height 24)
  (telephone-line-evil-use-short-tag t)  
  :config
  (setq telephone-line-faces '((evil . telephone-line-modal-face)
			       (modal . telephone-line-modal-face)
			       (ryo . telephone-line-ryo-modal-face)
			       (accent telephone-line-accent-active . telephone-line-accent-inactive)
			       (nil mode-line . mode-line-inactive)
			       (winum . (winum-face . winum-face))))
  (telephone-line-defsegment telephone-line-org-clock-segment ()
    (when (telephone-line-selected-window-active)
      (if (and (functionp 'org-clocking-p) (org-clocking-p))
	  (org-clock-get-clock-string))))
  (telephone-line-defsegment telephone-line-pdf-segment ()
    (when (eq major-mode 'pdf-view-mode)
      (propertize (pdf-view-page-number)
		  'face '(:inherit)
		  'display '(raise 0.0)
		  'mouse-face '(:box 1))))
  (telephone-line-defsegment telephone-line-winum-segment ()
    (propertize (eval (cadr winum--mode-line-segment))
		'face '(:box (:line-width 2 :color "cyan" :style released-button))		
		'display '(raise 0.0)
		'mouse-face '(:box 1)))
  (telephone-line-defsegment telephone-line-battery-segment ()
    (when (telephone-line-selected-window-active)
      (propertize battery-mode-line-string
		  'mouse-face '(:box 1))))

  (setq telephone-line-lhs '((winum . (telephone-line-winum-segment))
			     (accent . (telephone-line-pdf-segment
					telephone-line-vc-segment
					telephone-line-erc-modified-channels-segment
					telephone-line-process-segment))
			     (nil . (telephone-line-projectile-segment
				     telephone-line-buffer-segment
				     telephone-line-org-clock-segment
				     ))))
  (setq telephone-line-center-rhs '((evil . (telephone-line-battery-segment))))
  (setq telephone-line-rhs '((nil . (telephone-line-flycheck-segment					
				     ))
			     (accent . (telephone-line-major-mode-segment))
			     (evil . (telephone-line-airline-position-segment))))
  (telephone-line-mode t))

(use-package all-the-icons)

(use-package doom-themes

  :config
  (require 'doom-themes)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
	doom-themes-enable-italic t) ; if nil, italics is universally disabled
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  (doom-themes-org-config))

;; comes with emacs, but still GOLDEN PACKAGE for anyone who works on multiple servers
(use-package tramp
  :custom
  (tramp-default-method "ssh")
  (tramp-completion-reread-directory-timeout nil)
  (vc-handled-backends '(Git))

  ;; https://github.com/emacs-helm/helm/issues/981
  :config
  (add-to-list 'tramp-default-proxies-alist
	       '(nil "\\`root\\'" "/ssh:%h:"))
  (add-to-list 'tramp-default-proxies-alist
	       '((regexp-quote (system-name)) nil nil))
  (defun tsa/tramp-ibuffer-cleanup ()
    (interactive)
    (tramp-cleanup-all-buffers)
    (ibuffer-update nil))
  (bind-keys
   :map ibuffer-mode-map
   ("G" . tramp-ibuffer-cleanup)))

(use-package undo-tree

  :delight undo-tree-mode
  :bind (("C-x /" . undo-tree-visualize))
  :config
  (global-undo-tree-mode t))

(use-package unicode-fonts
  )

(use-package web-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode)))

(use-package eww
  :bind (("C-c M-e" . eww)))

(use-package eww-lnum
  :after eww
  :config
  (progn (define-key eww-mode-map "f" 'eww-lnum-follow)
	 (define-key eww-mode-map "F" 'eww-lnum-universal)))

(use-package wgrep)

(use-package winum
   :demand t
  :bind (("s-<tab>" . tsa/winum-or-switch))
  :config
  (winum-set-keymap-prefix (kbd "s-`"))
  (winum-mode t)
  (defun tsa/winum-or-switch (&optional p)
    (interactive "p")
    (if (= 2 winum--window-count)
	(other-frame p)
      (call-interactively 'winum-select-window-by-number))))

(use-package yasnippet
  :delight yas-minor-mode	   
  :config
  (add-to-list 'yas-snippet-dirs "~/emacs/Snippets")
  (add-to-list 'yas-snippet-dirs "~/emacs/.emacs.d/snippets/yasnippet-snippets/snippets")
  (use-package clojure-snippets )
  (yas-global-mode))

(defun tsa/getmail ()
  (interactive)
  (message "Getting mail...")
  (shell-command "~/bin/get-mail &" nil nil))

(use-package gnus
  :demand
  :bind (:map gnus-group-mode-map
	      ;; ([(meta tab)] . iflipb-next-buffer)
	      ;; ("\M-\C-i" . iflipb-next-buffer)
	      ;; ([M-tab] . iflipb-next-buffer)
	      ;; ("M-S-TAB" . iflipb-previous-buffer)
	      ("q" . tsa/iflip)
	      :map gnus-summary-mode-map
	      ;; ([M-tab] . iflipb-next-buffer)
	      ;; ("M-TAB" . iflipb-next-buffer)
	      ;; ("M-S-TAB" . iflipb-previous-buffer)
	      ("E" . gnus-summary-edit-article)
	      ("e" . gnus-summary-put-mark-as-expirable-next)
	      ("p" . gnus-summary-prev-article)
	      ("P" . gnus-summary-prev-unread-article)
	      ("n" . gnus-summary-next-article)
	      ("N" . gnus-summary-next-unread-article)
	      :map gnus-article-mode-map
	      ;([M-tab] . iflipb-next-buffer)
	      ("C-c C-f" . gnus-summary-mail-forward))
  :config
  (tsa/safe-load-file "~/.gnus")
  ;; Message delaying (scheduling) with C-c C-j
  (gnus-delay-initialize)
  (setq message-draft-headers '(References From)
	message-completion-alist
	'(("^\\(Newsgroups\\|Followup-To\\|Posted-To\\|Gcc\\):" . message-expand-group)
	  ("^\\(Resent-\\)?\\(To\\|B?Cc\\):" . tsa/message-expand-name)
	  ("^\\(Reply-To\\|From\\|Mail-Followup-To\\|Mail-Copies-To\\):" . tsa/message-expand-name)
	  ("^\\(Disposition-Notification-To\\|Return-Receipt-To\\):" . tsa/message-expand-name))
	nnir-method-default-engines '((nnml . notmuch)
				      (nnimap . imap)
				      (nntp . gmane)))
  (gnus-demon-add-handler 'gnus-delay-send-queue 1 nil)
  (gnus-demon-init)
  ;; end message delaying

  (add-to-list 'display-buffer-alist '("*Async Shell Command*" display-buffer-no-window))
  (with-eval-after-load 'gnus-topic
    (define-key gnus-topic-mode-map [remap gnus-topic-indent] 'gnus-topic-select-group)))

(defvar tsa-keys-map
    (let ((map (make-sparse-keymap)))
      (define-key map [remap just-one-space] 'cycle-spacing)
					  ;(define-key map (kbd "C-S-f7") 'column-highlight-mode)
      (define-key map (kbd "s-`") 'other-frame)
      (define-key map (kbd "<f1>") 'tsa/hydra-fkeys/body)
      (define-key map (kbd "<f5>") 'toggle-truncate-lines)
      (define-key map (kbd "<f7>") 'elfeed)
      (define-key map (kbd "<f8>") 'tsa/go-or-make-agenda) ;; Check agenda
      (define-key map (kbd "<C-f8>") 'calendar)
      (define-key map (kbd "<f9>") 'tsa/quick-gnus) ;; Check mail
      (define-key map (kbd "<f10>") 'tsa/go-or-make-bookmark-list) ;; Resume bookmark view
      (define-key map (kbd "<f11>") 'tsa/ivy-select-org-capture-template)
      (define-key map (kbd "<C-f11>") 'calculator)
      (define-key map (kbd "C-x C-d") 'dired) ;; so dired is both C-x C-d and C-x d
      (define-key map (kbd "C-x C-q") 'view-mode) ;; view mode
      (define-key map (kbd "M-C-;") 'comment-box)
      (define-key map (kbd "<C-f4>") 'tsa/yourls-shorten-at-point)
      (define-key map (kbd "C-c e") 'ebib)
      (define-key map (kbd "C-c r") 'ivy-bibtex)
      (define-key map (kbd "s-m") 'counsel-notmuch)
;      (define-key map (kbd "C-;") 'hydra-multiplecursors)
      (define-key map [(meta tab)] 'iflipb-next-buffer)
      (define-key map "\M-\C-i" 'iflipb-next-buffer)
      (define-key map (kbd "M-C-i") 'iflipb-next-buffer)
      (define-key map (kbd "M-TAB") 'iflipb-next-buffer)
      (define-key map [M-tab] 'iflipb-next-buffer)  
      (define-key map [M-tab] 'iflipb-next-buffer)
      (define-key map (kbd "M-S-TAB") 'iflipb-previous-buffer)
      (define-key map (kbd "C-c SPC") 'ace-jump-mode)
      (define-key map (kbd "M-c") 'capitalize-dwim)
      (define-key map (kbd "M-l") 'downcase-dwim)
      (define-key map (kbd "M-u") 'upcase-dwim)
      (define-key map (kbd "C-c h m") 'man)
      (define-key map (kbd "C-h c") 'counsel-colors-web)
      (define-key map (kbd "C-h u") 'counsel-unicode-char)
      (define-key map (kbd "C-h l") 'counsel-find-library)
      (define-key map (kbd "C-h v") 'counsel-describe-variable)
      (define-key map (kbd "C-h f") 'counsel-describe-function)
      (define-key map (kbd "C-h t") 'list-timers)
      (define-key map (kbd "C-h i") 'counsel-info-lookup-symbol)
      (define-key map (kbd "s-1") 'winum-select-window-1)
      (define-key map (kbd "s-2") 'winum-select-window-2)
      (define-key map (kbd "s-3") 'winum-select-window-3)
      (define-key map (kbd "s-4") 'winum-select-window-4)
      (define-key map (kbd "s-5") 'winum-select-window-5)
      (define-key map (kbd "s-6") 'winum-select-window-6)
      (define-key map (kbd "s-7") 'winum-select-window-7)
      (define-key map (kbd "s-8") 'winum-select-window-8)
      (define-key map (kbd "s-9") 'winum-select-window-9)
      (define-key map (kbd "s-<backspace>") 'kill-current-buffer)
      map)
    "my-keys-minor-mode keymap.")

  (define-minor-mode tsa-keys
    "A minor mode so that my key settings override annoying major modes."
    :init-value t
    :lighter " ")

  (global-set-key (kbd "<C-f3>") 'tsa-keys)
  (tsa-keys 1)

(add-hook 'after-load-functions 'my-keys-have-priority)

(defun my-keys-have-priority (_file)
  "Try to ensure that my keybindings retain priority over other minor modes.

Called via the `after-load-functions' special hook."
  (unless (eq (caar minor-mode-map-alist) 'tsa-keys)
    (let ((mykeys (assq 'tsa-keys minor-mode-map-alist)))
      (assq-delete-all 'tsa-keys minor-mode-map-alist)
      (add-to-list 'minor-mode-map-alist mykeys))))

(use-package exwm
    :init
;    (use-package exwm-config)
    (require 'exwm-randr)
    (setq exwm-workspace-show-all-buffers t)
    (setq exwm-layout-show-all-buffers t)
    (exwm-enable)
    (tsa/safe-load-file "~/local-config.el")
    (exwm-randr-enable) ; https://github.com/ch11ng/exwm/wiki
    (require 'exwm-systemtray)
    (exwm-systemtray-enable)
    :config
    (add-hook 'exwm-update-class-hook
	      (lambda ()  (unless (or (string-prefix-p "sun-awt-X11-" exwm-instance-name)  (string= "gimp" exwm-instance-name))
			    (exwm-workspace-rename-buffer exwm-class-name))))
    (defun tsa/exwm-class-name ()
      (let* ((n (concat exwm-class-name " : " exwm-title)))
	(cond
	 ((< 12 (length n)) (concat
			     (substring exwm-class-name 0 1)
			     " : "
			     (substring exwm-title 0 (min 20 (length exwm-title)))))
	 (t n))))


    (defun tsa/exwm-rename ()
      (interactive)
      (exwm-workspace-rename-buffer (tsa/exwm-class-name)))

    (add-hook 'exwm-update-title-hook 'tsa/exwm-rename))

(setq tsa/default-simulation-keys
      '(
	;; move
	([?\C-b] . left)
	([?\M-b] . C-left)
	([?\C-f] . right)
	([?\M-f] . C-right)
	([?\C-p] . up)
	([?\C-n] . down)
	([?\M-<] . C-home)
	([?\M->] . C-end)
	([?\C-a] . home) 
	([?\C-e] . end)
	([?\M-v] . prior)
	([?\C-v] . next)
	;; delete
	([?\C-d] . delete)
	([?\C-k] . (S-end delete))
	([?\M-d] . (C-S-right delete))
	;; cut/copy/paste.
	([?\C-w] . ?\C-x)
	([?\M-w] . ?\C-c)
	([?\C-y] . ?\C-v)
	([?\C-/] . ?\C-z)
	([?\M-/] . ?\C-y)
	;; search
	([?\C-s] . ?\C-f)))
(exwm-input-set-simulation-keys tsa/default-simulation-keys)
(exwm-input-set-key (kbd "C-z") 'hydra-shells/body)
(exwm-input-set-key (kbd "s-r") 'exwm-reset)
(exwm-input-set-key (kbd "s-k") 'exwm-input-toggle-keyboard)
(exwm-input-set-key (kbd "s-n") 'tsa/exwm-rename)
(exwm-input-set-key (kbd "s-N") 'rename-buffer)
(exwm-input-set-key (kbd "s-w") #'exwm-workspace-switch)
(exwm-input-set-key (kbd "s-f")
		    (lambda (&optional p)
		      (interactive "P")
		      (if p
			  (start-process-shell-command "firefox-private" nil "firefox --private-window http://google.com")
			(start-process-shell-command "firefox" nil "firefox"))))
(exwm-input-set-key (kbd "s-t")
		    (lambda ()
		      (interactive)
		      (start-process-shell-command "Telegram" nil "Telegram")))
(exwm-input-set-key (kbd "s-<f7>") (lambda () (interactive)
				     (let ((default-directory "/home/torysa/")) 
				       (shell-command (executable-find "touchpad_toggle")))))
(exwm-input-set-key (kbd "C-c o") 'hydra-global-org/body)
(exwm-input-set-key (kbd "C-M-o") 'hydra-window/body)
(exwm-input-set-key (kbd "s-l") (lambda () (interactive) (start-process-shell-command "lockscreen" nil "lockscreen")))
(exwm-input-set-key (kbd "s-e") (lambda () (interactive) (start-process-shell-command "dolphin" nil "dolphin")))
(exwm-input-set-key (kbd "s-g") (lambda (&optional p)
				  (interactive "P")
				  (if p
				      (start-process-shell-command "chrome-private" nil "google-chrome --incognito")
				    (start-process-shell-command "chrome" nil "google-chrome"))))
(exwm-input-set-key (kbd "s-G") (lambda () (interactive) (async-shell-command (executable-find "gwenview"))))
(exwm-input-set-key (kbd "C-`") 'hide-mode-line-mode)
(exwm-input-set-key (kbd "C-~") 'global-hide-mode-line-mode)
(exwm-input-set-key (kbd "<f1>") 'tsa/hydra-fkeys/body)
(exwm-input-set-key (kbd "<f8>") 'tsa/go-or-make-agenda)
(exwm-input-set-key (kbd "<f11>") 'tsa/ivy-select-org-capture-template)
(exwm-input-set-key (kbd "<f9>") 'tsa/quick-gnus)
(exwm-input-set-key (kbd "<XF86AudioLowerVolume>") (lambda () (interactive) (shell-command "amixer set Master 2%-")))
(exwm-input-set-key (kbd "<XF86AudioRaiseVolume>") (lambda () (interactive) (shell-command "amixer set Master 2%+")))
(exwm-input-set-key (kbd "<XF86AudioMute>") (lambda () (interactive) (shell-command "amixer set Master 1+ toggle")))
(exwm-input-set-key (kbd "<XF86MonBrightnessDown>") (lambda () (interactive) (shell-command "light -U 5; light")))
(exwm-input-set-key (kbd "<XF86MonBrightnessUp>") (lambda () (interactive) (shell-command "light -A 5; light")))
(exwm-input-set-key (kbd "<print>") (lambda () (interactive) (start-process-shell-command "flameshot gui" nil "flameshot gui")))
(exwm-input-set-key (kbd "s-<f9>") 'tsa/getmail)
(exwm-input-set-key (kbd "M-<tab>") 'iflipb-next-buffer)
(exwm-input-set-key (kbd "s-<tab>") 'tsa/winum-or-switch)
;(exwm-input-set-key (kbd "s-`") 'other-frame)
(exwm-input-set-key (kbd "<XF86PowerOff>") (lambda () (interactive) (message "Power Press")))
(exwm-input-set-key (kbd "M-<iso-lefttab>") 'iflipb-previous-buffer)
(exwm-input-set-key (kbd "s-1") 'winum-select-window-1)
(exwm-input-set-key (kbd "s-2") 'winum-select-window-2)
(exwm-input-set-key (kbd "s-3") 'winum-select-window-3)
(exwm-input-set-key (kbd "s-4") 'winum-select-window-4)
(exwm-input-set-key (kbd "s-5") 'winum-select-window-5)
(exwm-input-set-key (kbd "s-6") 'winum-select-window-6)
(exwm-input-set-key (kbd "s-7") 'winum-select-window-7)
(exwm-input-set-key (kbd "s-8") 'winum-select-window-8)
(exwm-input-set-key (kbd "s-9") 'winum-select-window-9)
(exwm-input-set-key (kbd "s-m") 'counsel-notmuch)
(exwm-input-set-key (kbd "s-<backspace>") 'kill-this-buffer)

(load-file custom-file)

(window-divider-mode 1)

(load-theme 'doom-challenger-deep)

(tsa/go-or-make-agenda)
